# Netty+JavaFx实战：仿桌面版微信聊天
## 一、工程源码

**源码地址**：
- [https://github.com/fuzhengwei/NaiveChat](https://github.com/fuzhengwei/NaiveChat)
- [https://gitee.com/itstack/NaiveChat](https://gitee.com/itstack/NaiveChat)

## 二、功能概述

本专栏会以三个大章节内容，逐步进行讲解；

**第一章节**：**UI开发**。使用```JavaFx```与```Maven```搭建UI桌面工程，逐步讲解登录框体、聊天框体、对话框、好友栏等各项UI展示及操作事件。从而在这一章节中让Java 程序员学会开发桌面版应用。

**第二章节**：**架构设计**。在这一章节中我们会使用DDD领域驱动设计的四层模型结构与Netty结合使用，架构出合理的分层框架。同时还有相应库表功能的设计。相信这些内容学习后，你一定也可以假设出更好的框架。

**第三章节**：**功能实现**。这部分我们主要将通信中的各项功能逐步实现，包括；登录、添加好友、对话通知、消息发送、断线重连等各项功能。最终完成整个项目的开发，同时也可以让你从实践中学会技能。

---

![](https://bugstack.cn/assets/images/2020/p-xmind.png)

## 三、项目演示

>登陆页面

![登陆页面](https://bugstack.cn/assets/images/2020/ui-00.png)

>聊天页面

![聊天页面](https://bugstack.cn/assets/images/2020/ui-01.png)

>添加好友

![添加好友](https://bugstack.cn/assets/images/2020/ui-02.png)

>消息提醒

![消息提醒](https://bugstack.cn/assets/images/2020/ui-05.png)

## 四、专栏学习（自愿支持）

>专栏共有25篇文章，分别从UI、架构到功能实现逐步讲解，非常适合新人学习提升编码能力和架构思想。

学习链接：[chat.itstack.org](https://chat.itstack.org)

![](https://bugstack.cn/assets/images/2020/invite.jpg)


- **公众号**
沉淀、分享、成长，专注于原创专题案例，以最易学习编程的方式分享知识，让自己和他人都能有所收获。目前已完成的专题有；Netty4.x实战专题案例、用Java实现JVM、基于JavaAgent的全链路监控、手写RPC框架、DDD专题案例、源码分析等。
<img src="https://itstack.org/_media/qrcode.png?x-oss-process=style/may" width="180" height="180"/>

## 六、参与贡献

1. 如果您对本项目有任何建议或发现文中内容有误的，欢迎提交 issues 进行指正。
2. 对于文中我没有涉及到知识点，欢迎提交 PR。
